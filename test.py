
# from autopair_autocorrectml.vsms import Vsms
from autopair_autocorrectml.corrector import Corrector


print('hello world')

corrector = Corrector()
output = corrector.query_car_brand('โตโยด่ะ')
print(output)
print(corrector.query_car_model('SERIE3'))
print(corrector.query_item_name('ไส้กรองน้ำมันเครื่อง'))
print(corrector.query_item_name('ลูกหมาชัก'))
print(corrector.query_item_brand('333'))
print(corrector.query_item_brand('หมี'))
print(corrector.query_fitment('หน้า'))
print(corrector.query_fitment('ซ้าย หน้า'))
print(corrector.query_fitment('หน้าซ้าย ขวา'))
print(corrector.query_fitment('คู่หน้า'))
print(corrector.query_fitment('คู่หน้าล่าง'))
